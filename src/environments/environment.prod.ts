export const environment = {
  production: true,
  api_root: 'https://us-east1-will-play-app.cloudfunctions.net/api'
};
