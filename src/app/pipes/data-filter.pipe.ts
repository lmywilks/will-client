import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataFilter'
})
export class DataFilterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (args.length === 1) {
      if (args[0]) {
        return value.filter(x => x.indexOf(args[0]) > -1);
      }
    } else if (args.length === 2) {
      if (args[0] && args[1]) {
        if (args[0] === 'createdAt') {
          return value.filter(x => {
            switch (args[1]) {
              case 'today':
                return new Date(x[args[0]]).getDate() === new Date().getDate();
              case 'yesterday':
                return new Date(x[args[0]]).getDate() === new Date(new Date().getTime() - 24 * 60 * 60 * 1000).getDate();
              case 'this week':
                const day = new Date().getDay();
                return new Date(x[args[0]]).getTime() >= new Date().setMilliseconds(0) - 24 * 60 * 60 * 1000 * day;
              case 'this month':
                return new Date(x[args[0]]).getMonth() === new Date().getMonth(); 
              case 'this year':
                return new Date(x[args[0]]).getFullYear() === new Date().getFullYear(); 
              default:
                return true;
            }
          });
        } else {
          return value.filter(x => x[args[0]] === args[1]);
        }
      }
    } else if (args.length === 3) {
      if (args[0] && args[1] && args[2]) {
        return value.filter(x => x[args[0]].indexOf(args[1]) > -1);
      }
    }

    return value;
  }

}
