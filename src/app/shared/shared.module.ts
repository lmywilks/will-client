import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnimationService, AnimatorModule } from 'css-animator';
import { ClickOutsideModule } from 'ng-click-outside';
import { FileUploadModule } from 'ng2-file-upload';
import { NgbTabsetModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ModalAnimationComponent } from './modal-animation/modal-animation.component';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [
    ModalAnimationComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    ClickOutsideModule,
    NgbTabsetModule,
    AnimatorModule,
    NgbModule,
    FileUploadModule
  ],
  exports: [
    ClickOutsideModule,
    ModalAnimationComponent,
    CardComponent,
    FileUploadModule,
    NgbTabsetModule,
    AnimatorModule,
    NgbModule
  ],
  providers: [AnimationService]
})
export class SharedModule { }
