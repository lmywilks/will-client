import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private readonly API = environment.api_root;

  constructor(private http: HttpClient) { }

  List() {
    return this.http.get(`${ this.API }/todo`);
  }

  Create(newTodo: any) {
    return this.http.post(`${ this.API }/todo`, newTodo);
  }

  Update(todo: any) {
    return this.http.put(`${ this.API }/todo/${ todo.groupId }`, todo);
  }

  Delete(groupId: string) {
    return this.http.delete(`${ this.API }/todo/${ groupId }`);
  }

  UpdateList(groupId: string, list: any) {
    return this.http.put(`${ this.API }/todo/${ groupId }/list`, list);
  }

}
