import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly API = environment.api_root;

  private currentTokenSubject: BehaviorSubject < any > ;
  public currentToken: Observable < any > ;

  private currentUserSubject: BehaviorSubject < any > ;
  public currentUser: Observable < any > ;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router
  ) { 
    this.currentTokenSubject = new BehaviorSubject < any > (localStorage.getItem('token'));
    this.currentToken = this.currentTokenSubject.asObservable();

    this.currentUserSubject = new BehaviorSubject < any > (localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentTokenValue(): any {
    return this.currentTokenSubject.value;
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  public Signup(userData: any) {
    return this.http.post(`${ this.API }/signup`, userData)
      .pipe(map((res: any) => {
        localStorage.setItem('token', res.token);
        this.currentTokenSubject.next(res.token);
        return this.RetrieveUser();
      }));
  }

  public Login(user: any) {
    return this.http.post(`${ this.API }/login`, { email: user.email, password: user.password })
    .pipe(map((res: any) => {
      localStorage.setItem('token', res.token);

      if (user.remember) {
        this.cookieService.set('loginForm', JSON.stringify(user));
      } else {
        this.cookieService.delete('loginForm');
      }

      this.currentTokenSubject.next(res.token);

      return this.RetrieveUser();
    }));
  }

  public RetrieveUser() {
    return this.http.get(`${ this.API }/user`)
      .pipe(map((res: any) => {
        this.currentUserSubject.next(res);
        return res;
      }));
  }

  public UpdateUserDetail(userDetail) {
    return this.http.post(`${ this.API }/user`, userDetail);
  }

  public Logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.currentTokenSubject.next(null);
    this.currentUserSubject.next(null);
    this.router.navigate(['/login']);
  }

}
