import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private readonly API = environment.api_root;

  constructor(private http: HttpClient) { }

  List() {
    return this.http.get(`${ this.API }/project`);
  }

  Retrieve(id) {
    return this.http.get(`${ this.API }/project/${ id }`);
  }

  Create(newProject: any) {
    return this.http.post(`${ this.API }/project`, newProject);
  }

  Update(project: any) {
    return this.http.put(`${ this.API }/project/${ project.projectId }`, project);
  }

  Delete(projectId: string) {
    return this.http.delete(`${ this.API }/project/${ projectId }`);
  }

  AddTask(projectId: string, task: any) {
    return this.http.post(`${ this.API }/project/${ projectId }/task`, task);
  }

  DeleteTask(projectId: string, taskId: string) {
    return this.http.delete(`${ this.API }/project/${ projectId }/task/${ taskId }`);
  }

  UpdateTask(projectId: string, task: any) {
    return this.http.put(`${ this.API }/project/${ projectId }/task/${ task.taskId }`, task);
  }
}
