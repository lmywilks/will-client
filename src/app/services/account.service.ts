import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private readonly API = environment.api_root;

  constructor(private http: HttpClient) { }

  List() {
    return this.http.get(`${ this.API }/account`);
  }

  Create(newAccount: any) {
    return this.http.post(`${ this.API }/account`, newAccount);
  }

  Update(account: any) {
    return this.http.put(`${ this.API }/account/${ account.accountId }`, account);
  }

  Delete(accountId: string) {
    return this.http.delete(`${ this.API }/account/${ accountId }`);
  }
}
