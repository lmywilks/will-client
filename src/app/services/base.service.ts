import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  private readonly API = environment.api_root;

  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  Dashboard() {
    return this.http.get(`${ this.API }/dashboard`);
  }
}
