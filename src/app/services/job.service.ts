import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  private readonly API = environment.api_root;

  constructor(private http: HttpClient) { }

  List() {
    return this.http.get(`${ this.API }/job`);
  }

  Create(newJob: any) {
    return this.http.post(`${ this.API }/job`, newJob);
  }

  Update(job: any) {
    return this.http.put(`${ this.API }/job/${ job.jobId }`, job);
  }

  Delete(jobId: string) {
    return this.http.delete(`${ this.API }/job/${ jobId }`);
  }
  
}
