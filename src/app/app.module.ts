import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { NgbButtonsModule, NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './pages/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { NavContentComponent } from './layout/navigation/nav-content/nav-content.component';
import { NavLogoComponent } from './layout/navigation/nav-logo/nav-logo.component';
import { NavCollapseComponent } from './layout/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavGroupComponent } from './layout/navigation/nav-content/nav-group/nav-group.component';
import { NavItemComponent } from './layout/navigation/nav-content/nav-item/nav-item.component';
import { NavigationItem } from './layout/navigation/navigation';
import { NavBarComponent } from './layout/navigation/nav-bar/nav-bar.component';
import { NavLeftComponent } from './layout/navigation/nav-bar/nav-left/nav-left.component';
import { NavRightComponent } from './layout/navigation/nav-bar/nav-right/nav-right.component';
import { ToggleFullScreenDirective } from './shared/full-screen/toggle-full-screen';
import { NavSearchComponent } from './layout/navigation/nav-bar/nav-left/nav-search/nav-search.component';
import { BasicAuthInterceptor } from './utils/base-auth.interceptor';
import { ErrorInterceptor } from './utils/error.interceptor';
import { UserService } from './services/user.service';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { LoadingInterceptor } from './utils/loading.interceptor';
import { LoadingComponent } from './utils/loading/loading.component';
import { FooterComponent } from './layout/footer/footer.component';
import { AboutComponent } from './pages/about/about.component';

import { AppInitService } from './utils/app-init.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

export function initializeApp(appInitService: AppInitService) {
  return (): Promise<any> => { 
    return appInitService.Init();
  }
}

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    DashboardComponent,
    NavigationComponent,
    NavContentComponent,
    NavLogoComponent,
    NavCollapseComponent,
    NavGroupComponent,
    NavItemComponent,
    NavBarComponent,
    NavLeftComponent,
    NavRightComponent,
    ToggleFullScreenDirective,
    NavSearchComponent,
    BreadcrumbComponent,
    LoadingComponent,
    FooterComponent,
    AboutComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbButtonsModule,
    PerfectScrollbarModule
  ],
  providers: [
    UserService,
    CookieService,
    AppInitService,
    { 
      provide: APP_INITIALIZER,
      useFactory: initializeApp, 
      deps: [AppInitService], 
      multi: true
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    NavigationItem,
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS,  useClass: LoadingInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
