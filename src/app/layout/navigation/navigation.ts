import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-navigation',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        url: '/dashboard/home',
        icon: 'feather icon-home',
        classes: 'nav-item'
      },
      {
        id: 'account',
        title: 'Account',
        type: 'item',
        url: '/dashboard/account',
        icon: 'fas fa-wallet',
        classes: 'nav-item'
      },
      {
        id: 'team',
        title: 'Team',
        type: 'item',
        url: '/dashboard/team',
        icon: 'fas fa-users',
        classes: 'nav-item'
      },
      {
        id: 'project',
        title: 'Project',
        type: 'item',
        url: '/dashboard/project',
        icon: 'feather icon-clipboard',
        classes: 'nav-item'
      },
      {
        id: 'todo',
        title: 'Todo List',
        type: 'item',
        url: '/dashboard/todo',
        icon: 'feather icon-list',
        classes: 'nav-item'
      },
      {
        id: 'restaurant',
        title: 'Restaurant',
        type: 'item',
        url: '/dashboard/restaurant',
        icon: 'fas fa-store',
        classes: 'nav-item'
      }
    ]
  },
  // {
  //   id: 'settings',
  //   title: 'Settings',
  //   type: 'group',
  //   icon: 'feather icon-settings',
  //   children: [
  //     {
  //       id: 'profile',
  //       title: 'Profile',
  //       type: 'item',
  //       url: '/dashboard/profile',
  //       icon: 'feather icon-file-text',
  //       classes: 'nav-item'
  //     },
  //     {
  //       id: 'settings',
  //       title: 'Settings',
  //       type: 'item',
  //       url: '/dashboard/settings',
  //       icon: 'feather icon-settings',
  //       classes: 'nav-item'
  //     }
  //   ]
  // },
  {
    id: 'system',
    title: 'System',
    type: 'group',
    icon: 'feather icon-cpu',
    children: [
      {
        id: 'about',
        title: 'About',
        type: 'item',
        url: '/about',
        icon: 'feather icon-external-link',
        classes: 'nav-item',
        target: true
      }
    ]
  }
];

@Injectable()
export class NavigationItem {
  get() {
    return NavigationItems;
  }
}
