import { Component, OnInit } from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {animate, style, transition, trigger} from '@angular/animations';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-nav-right',
  templateUrl: './nav-right.component.html',
  styleUrls: ['./nav-right.component.scss'],
  providers: [NgbDropdownConfig],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('300ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('300ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class NavRightComponent implements OnInit {

  public user: any;
  public notifications: any;

  public visibleUserList: boolean;
  public chatMessage: boolean;
  public friendId: boolean;

  constructor(
    config: NgbDropdownConfig,
    private userService: UserService
  ) {
    config.placement = 'bottom-right';
    this.visibleUserList = false;
    this.chatMessage = false;

    setTimeout(() => {
      this.userService.currentUser.subscribe((res: any) => {
        this.user = res;
      });
    }, 3000);

  }

  ngOnInit() {}

  onChatToggle(friend_id) {
    this.friendId = friend_id;
    this.chatMessage = !this.chatMessage;
  }
  
  logout() {
    this.userService.Logout();
  }
}
