import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {WillConfig} from '../../app-config';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onNavCollapse = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onNavCollapsedMob = new EventEmitter();
  public willConfig: any;
  public navCollapsed;
  public navCollapsedMob;
  public windowWidth: number;

  constructor() {
    this.willConfig = WillConfig.config;
    this.windowWidth = window.innerWidth;
    this.navCollapsed = (this.windowWidth >= 992) ? this.willConfig['collapse-menu'] : false;
    this.navCollapsedMob = false;
  }

  ngOnInit() {
  }

  navCollapse() {
    if (this.windowWidth >= 992) {
      this.navCollapsed = !this.navCollapsed;
      this.onNavCollapse.emit();
    }
  }

  navCollapseMob() {
    if (this.windowWidth < 992) {
      this.onNavCollapsedMob.emit();
    }
  }

}
