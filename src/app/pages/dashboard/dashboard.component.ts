import { Component, OnInit } from '@angular/core';
import { WillConfig } from '../../app-config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public willConfig: any;
  public navCollapsed: boolean;
  public navCollapsedMob: boolean;
  public windowWidth: number;

  constructor() {
    this.willConfig = WillConfig.config;

    this.windowWidth = window.innerWidth;
    this.navCollapsed = (this.windowWidth >= 992) ? this.willConfig['collapse-menu'] : false;
    this.navCollapsedMob = false;
  }

  ngOnInit() {

  }

}
