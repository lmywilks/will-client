import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';

import { TagInputModule } from 'ngx-chips';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTableModule } from 'angular2-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxEditorModule } from 'ngx-editor';

import { DataFilterPipe } from '../../../pipes/data-filter.pipe';

import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account/account.component';
import { SettingComponent } from './setting/setting.component';
import { TodoComponent } from './todo/todo.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { ProjectComponent } from './project/project.component';
import { TeamComponent } from './team/team.component';
import { ProjectCardComponent } from './project/project-card/project-card.component';
import { ProjectDetailComponent } from './project/project-detail/project-detail.component';


@NgModule({
  declarations: [
    HomeComponent,
    ProfileComponent,
    AccountComponent,
    SettingComponent,
    TodoComponent,
    RestaurantComponent,
    ProjectComponent,
    TeamComponent,
    ProjectCardComponent,
    DataFilterPipe,
    ProjectDetailComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TagInputModule,
    NgxDatatableModule,
    NgbModule,
    DataTableModule,
    NgxEditorModule
  ]
})
export class HomeModule { }
