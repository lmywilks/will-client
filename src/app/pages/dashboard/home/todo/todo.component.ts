import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todo_list: any[] = [];
  selectedGroup: any;
  groupName: string;
  todoName: string;

  constructor(
    private todoService: TodoService
  ) { }

  ngOnInit() {
    this.todoService.List()
      .subscribe(
        (res: any) => this.todo_list = res,
        (err: any) => console.log(err)
      );
  }

  createGroup() {
    if (this.groupName && this.groupName.trim() != '') {
      this.todoService.Create({ name: this.groupName })
        .subscribe(
          (res: any) => {
            this.todo_list = [res, ...this.todo_list];
            this.groupName = '';
          },
          (err: any) => console.log(err)
        );
    }
  }

  updateGroup(item) {

  }

  deleteGroup(event, item, index) {
    event.stopPropagation();
    event.preventDefault();

    Swal.fire({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(res => {
        if (res.value) {
          if (item.groupId) {
            this.todoService.Delete(item.groupId)
              .subscribe(
                (res: any) => {
                  this.todo_list.splice(index, 1);
                  
                  Swal.fire(
                    'Deleted!',
                    'Your job has been deleted.',
                    'success'
                  );
                },
                (err: any) => console.log(err)
              )
          }
        }
      });
  }

  updateList(event, type, item, index) {

    event.stopPropagation();
    event.preventDefault();

    setTimeout(() => {
      let isUpdated: boolean = false;

      if (
        this.selectedGroup && 
        this.selectedGroup.groupId
      ) {
        switch (type) {
          case 'add':
            if (
              this.todoName && 
              this.todoName.trim() != ''
            ) {
              this.selectedGroup.list = [{ name: this.todoName, done: false }, ...this.selectedGroup.list];
              this.todoName = '';
              isUpdated = true;
            }
            break;
          case 'delete':
            if (index >= 0 && index != null && this.selectedGroup.list) {
              this.selectedGroup.list.splice(index, 1);
              isUpdated = true;
            }
            break;
          case 'done':
            if (item) {
              item.done = !item.done;
              isUpdated = true;
            }
            break;
        }
  
        if (isUpdated) {
          this.selectedGroup.done = this.selectedGroup.list.filter(x => !x.done).length === 0 ? true : false;
  
          this.todoService.UpdateList(this.selectedGroup.groupId, this.selectedGroup.list)
            .subscribe(
              (res: any) => {},
              (err: any) => console.log(err)
            );
        }
      }
    });
  }
  
}
