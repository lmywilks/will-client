import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../../../utils/auth.guard';

import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account/account.component';
import { SettingComponent } from './setting/setting.component';
import { TodoComponent } from './todo/todo.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { ProjectComponent } from './project/project.component';
import { TeamComponent } from './team/team.component';
import { ProjectDetailComponent } from './project/project-detail/project-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'settings',
        component: SettingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'todo',
        component: TodoComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'restaurant',
        component: RestaurantComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'project',
        component: ProjectComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'project/:projectId',
        component: ProjectDetailComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'team',
        component: TeamComponent,
        canActivate: [AuthGuard]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
