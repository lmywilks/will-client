import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { environment } from '../../../../../environments/environment';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  
  public profileForm: FormGroup;
  public user: any;

  public uploader:FileUploader = new FileUploader({
    url: `${ environment.api_root }/user/image`,
    itemAlias: 'photo',
    autoUpload: true,
    authToken: `Bearer ${ this.userService.currentTokenValue }`,
    authTokenHeader: 'Authorization'
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService
  ) {
    this.userService.currentUser.subscribe((user: any) => {
      this.user = user;

      if (!this.user) {
        setTimeout(() => {
          this.setData();
        }, 4000);
      } else {
        this.setData();
      }
    });
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
    //overide the onCompleteItem property of the uploader so we are 
    //able to deal with the server response.
    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
      this.user.credentials.imageUrl = JSON.parse(response).url;
    };
  }

  private setData() {
    this.profileForm = this.fb.group({
      username: [{ value: this.user ? this.user.credentials.username : '', disabled: true }],
      email: [{ value: this.user ? this.user.credentials.email : '', disabled: true }],
      company: [this.user.credentials.details.company || ''],
      firstName: [this.user.credentials.details.firstName || ''],
      lastName: [this.user.credentials.details.lastName || ''],
      address: [this.user.credentials.details.address || ''],
      city: [this.user.credentials.details.city || ''],
      country: [this.user.credentials.details.country || ''],
      postalCode: [this.user.credentials.details.postalCode || ''],
      facebook: [this.user.credentials.details.facebook || ''],
      twitter: [this.user.credentials.details.twitter || ''],
      linkedin: [this.user.credentials.details.linkedin || ''],
      description: [this.user.credentials.details.description || '']
    });
  }

  public buildUrl(url: string) {
    if (url.startsWith('http')) {
      return url;
    }

    return 'http://' + url;
  }

  public save() {
    this.userService.UpdateUserDetail(this.profileForm.value)
      .subscribe(
        (res: any) => {},
        (err: any) => console.log(err)
      );
  }

  public onChangeImage(event) {
    if (event && event.target && event.target.files && event.target.files.length) {
      let formData = new FormData();
      formData.append('photo', event.target.files.item(0));
    }
  }

}
