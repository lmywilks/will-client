import { Component, OnInit } from '@angular/core';

import { BaseService } from 'src/app/services/base.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public dashboard: any;

  private limit: any = {
    expense: {
      daily: 15,
      monthly: 50,
      yearly: 100
    }
  };

  currencyCode = '';

  constructor(
    private baseService: BaseService,
    private todoService: TodoService
  ) { }

  ngOnInit() {
    this.baseService.Dashboard()
      .subscribe(
        (res: any) => this.dashboard = res,
        (err: any) => console.log(err)
      );
  }

  getPercentage(value, frequency, type, isNumber:boolean=false) {
    return isNumber ? value * 100 / this.limit[type][frequency] : (value * 100 / this.limit[type][frequency]).toString() + '%';
  }

  calcTodoPercentage() {
    if (this.dashboard && this.dashboard.todo && this.dashboard.todo.list) {
      const list = this.dashboard.todo.list;

      const done_len = list.filter(x => x.done).length;
  
      return done_len / list.length;
    }
    
    return 0;
  }

  updateTodo(item) {
    if (this.dashboard.todo && this.dashboard.todo.groupId) {
      setTimeout(() => {
        item.done = !item.done;

        this.dashboard.todo.done = this.dashboard.todo.list.filter(x => !x.done).length === 0 ? true : false;

        this.todoService.UpdateList(this.dashboard.todo.groupId, this.dashboard.todo.list)
          .subscribe(
            (res: any) => {},
            (err: any) => console.log(err)
          )
      });
    }
  }
}
