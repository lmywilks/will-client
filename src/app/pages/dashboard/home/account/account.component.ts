import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { transition, trigger, style, animate } from '@angular/animations';
import Swal from 'sweetalert2';

import { AccountService } from 'src/app/services/account.service';
import { JobService } from 'src/app/services/job.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class AccountComponent implements OnInit {
  
  private now: Date = new Date();
  account: any = {};
  account_list: any[] = [];
  job_list: any[] = [];
  job: any = {};
  days: number[] = [];

  autocompleteItems = ['Food', 'Drink', 'Gas', 'Market', 'Salary'];

  constructor(
    private accountService: AccountService,
    private jobService: JobService
  ) {
    for(let i = 1; i <= 31; i++) {
      this.days.push(i);
    }
  }

  ngOnInit() {
    this.accountService.List()
      .subscribe(
        (res: any) => {
          this.account_list = res.accounts;
          this.job_list = res.jobs;
        },
        (err: any) => console.log(err)
      );
  }

  addAccount() {
    this.account = {
      date: {
        day: this.now.getDate(),
        month: this.now.getMonth() + 1,
        year: this.now.getFullYear()
      },
      type: 'Expense',
      category: [],
      name: '',
      money: 0
    };

    document.querySelector('#add-account').classList.add('md-show');
  }

  closeMyModal(id) {
    document.querySelector('#' + id).classList.remove('md-show');
  }

  saveAccount() {
    if (this.account.accountId) {
      this.accountService.Update(this.account)
        .subscribe(
          (res: any) => { 
            document.querySelector('#add-account').classList.remove('md-show');
          },
          (err: any) => console.log(err)
        )
    } else {
      this.accountService.Create(this.account)
        .subscribe(
          (res: any) => { 
            this.account_list = [res, ...this.account_list];
            document.querySelector('#add-account').classList.remove('md-show');
          },
          (err: any) => console.log(err)
        );
    }
  }

  editAccount(item) {
    this.account = item;

    document.querySelector('#add-account').classList.add('md-show');
  }

  deleteAccount(item, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(res => {
        if (res.value) {
          if (item.accountId) {
            this.accountService.Delete(item.accountId)
              .subscribe(
                (res: any) => {
                  this.account_list.splice(index, 1);
                  
                  Swal.fire(
                    'Deleted!',
                    'Your transaction has been deleted.',
                    'success'
                  );
                },
                (err: any) => console.log(err)
              )
          }
        }
      });
  }

  updateFilter(event) {}

  changeStatus(item) {}

  addJob() {
    this.job = {
      name: '',
      type: 'Income',
      category: [],
      jobType: 'account',
      status: 1,
      day: 1,
      money: 0
    };

    document.querySelector('#add-job').classList.add('md-show');
  }

  saveJob() {
    if (this.job.jobId) {
      this.jobService.Update(this.job)
        .subscribe(
          (res: any) => {
            document.querySelector('#add-job').classList.remove('md-show');
          },
          (err: any) => console.log(err)
        )
    } else {
      this.jobService.Create(this.job)
        .subscribe(
          (res: any) => {
            this.job_list = [res, ...this.job_list];
            document.querySelector('#add-job').classList.remove('md-show');
          },
          (err: any) => console.log(err)
        );
    }
  }

  editJob(job) {
    this.job = job;

    document.querySelector('#add-job').classList.add('md-show');
  }

  deleteJob(job, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(res => {
        if (res.value) {
          if (job.jobId) {
            this.jobService.Delete(job.jobId)
              .subscribe(
                (res: any) => {
                  this.job_list.splice(index, 1);
                  
                  Swal.fire(
                    'Deleted!',
                    'Your job has been deleted.',
                    'success'
                  );
                },
                (err: any) => console.log(err)
              )
          }
        }
      });
  }

}
