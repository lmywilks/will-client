import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  projects: any[] = [];
  newProject: any;
  user: any;
  teams: any[] = [];
  calc: any = {
    total: 0,
    status: {
      Completed: 0,
      Open: 0,
      'On Hold': 0,
      Rejected: 0
    },
    priority: {
      Highest: 0,
      High: 0,
      Normal: 0,
      Low: 0
    }
  };

  filter: any = {
    date: '',
    name: '',
    status: '',
    priority: ''
  };

  constructor(
    private projectService: ProjectService,
    private userService: UserService
  ) { 
    setTimeout(() => {
      this.userService.currentUser.subscribe((res: any) => {
        this.user = res;
      });
    }, 2000);
  }

  ngOnInit() {
    this.projectService.List()
      .subscribe(
        (res: any) => {
          this.projects = res.projects;
          this.teams = res.teams;

          this.updateCalc();  
        },
        (err: any) => console.log(err)
      );
  }

  addProject() {
    this.newProject = {
      name: '',
      eslimate: '',
      priority: 'Normal',
      status: 'Open',
      team: '',
      assignee: [
        {
          imageUrl: this.user.credentials.imageUrl,
          userId: this.user.credentials.userId,
          name: (
            this.user.credentials.details && 
            this.user.credentials.details.firstName || this.user.credentials.details.lastName
            ) ? (this.user.credentials.details.firstName + ' ' + this.user.credentials.details.lastName) : this.user.credentials.username
        }
      ]
    };

    document.querySelector('#add-project').classList.add('md-show');
  }

  saveProject() {
    this.projectService.Create(this.newProject)
      .subscribe(
        (res: any) => {
          this.projects = [res, ...this.projects];
          this.calc.total = this.projects.length;
          this.closeMyModal();
        },
        (err: any) => console.log(err)
      );
  }

  closeMyModal() {
    document.querySelector('#add-project').classList.remove('md-show');
  }

  calcWidth(value: number) {
    if (!this.calc.total || value === 0) {
      return '1%';
    } else {
      return (value * 100 / this.calc.total) +'%';
    }
  }

  updateFilter($event, type, value) {
    $event.preventDefault();

    this.filter[type] = value;
  }

  updateCalc() {
    this.calc.total = this.projects.length; 

    this.projects.forEach(p => {
      this.calc.status[p.status]++;
      this.calc.priority[p.priority]++;
    });
  }
}
