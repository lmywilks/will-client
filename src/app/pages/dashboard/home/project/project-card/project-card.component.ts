import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss'],
  providers: [NgbDropdownConfig],
})
export class ProjectCardComponent implements OnInit {

  @Input() project: any;
  @Input() index: number;
  @Input() list: any[];
  @Input() calc: any;
  
  constructor(
    config: NgbDropdownConfig,
    private projectService: ProjectService
  ) {
    config.placement = 'bottom-right';
  }

  ngOnInit() {
  }

  update(event, type, value) {
    event.stopPropagation();
    event.preventDefault();

    if (this.project[type] === value) {
      return;
    }

    this.project[type] = value;

    this.projectService.Update(this.project)
      .subscribe(
        (res : any) => {
          this.updateCalc(type);
        },
        (err: any) => console.log(err)
      );
  }

  remove(event, item) {
    event.stopPropagation();
    event.preventDefault();

    if (!item.projectId) {
      return;
    }

    Swal.fire({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(res => {
        if (res.value) {
          if (item.projectId) {
            this.projectService.Delete(item.projectId)
              .subscribe(
                (res: any) => {
                  this.list.splice(this.index, 1);
                  this.updateCalc('total');
                  Swal.fire(
                    'Deleted!',
                    'Your transaction has been deleted.',
                    'success'
                  );
                },
                (err: any) => console.log(err)
              );
          }
        }
      });
  }

  updateCalc(type) {

    switch (type) {
      case 'status':
      case 'priority':
        Object.keys(this.calc[type]).forEach(k => {
          this.calc[type][k] = 0;
        });

        this.list.forEach(p => {
          this.calc[type][p[type]]++;
        });
        break;
      case 'total':
          this.calc.total = this.list.length;
        break;
    }
  }

}
