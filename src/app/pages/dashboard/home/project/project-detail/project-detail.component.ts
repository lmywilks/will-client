import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import Swal from 'sweetalert2';

import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  project: any;
  tasks: any[] = [];
  isEditorFocus = false;
  desc = '';
  newTask: any = null;
  filter: any = {
    status: ''
  };

  diff: number;
  $counter: Observable<number>;
  subscription: Subscription;
  message: string = '0 years 0 days 0 hours 0 min 0 sec';
  dYears: number;
  dDays: number;
  dHours: number;
  dMinutes: number;
  dSeconds: number;

  editorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Enter description here...',
    translate: 'no',
    toolbar: [
      ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
      ["fontName", "fontSize", "color"],
      ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      ["removeFormat"],
      ["blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      ["link", "unlink"]
    ]
  };

  editMode = false;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(param => {
      if (param.projectId) {
        this.projectService.Retrieve(param.projectId)
          .subscribe(
            (res: any) => {
              this.project = res;
              if (
                this.project && 
                this.project.active && 
                this.project.checkIn && 
                this.project.status != 'Completed' && 
                this.project.status != 'Rejected'
              ) {
                this.startCounter();
              } else if (this.project && this.project.status === 'Completed' && this.project.completedAt) {
                this.diff = Math.abs(Math.floor((new Date(this.project.checkIn).getTime() - new Date(this.project.completedAt).getTime()) / 1000));
                this.message = this.dhms(this.diff);
              }
            },
            (err: any) => console.log(err)
          );
      }
    });
  }

  ngOnInit() {
    
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  startCounter() {
    this.$counter = Observable.interval(1000).map((x) => {
      this.diff = Math.abs(Math.floor((new Date(this.project.checkIn).getTime() - new Date().getTime()) / 1000));
      return x;
    });

    this.subscription = this.$counter.subscribe((x) => this.message = this.dhms(this.diff));
  }

  dhms(t) {
    let years = 0;
    let days = 0;
    let hours = 0;
    let minutes = 0;
    let seconds = 0;
    days = Math.floor(t / 86400);
    if (days > 365) {
      years = Math.floor(days / 365);
      days = days - (years * 365);
    }
    t -= days * 86400;
    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;

    this.dYears = years;
    this.dDays = days;
    this.dHours = hours;
    this.dMinutes = minutes;
    this.dSeconds = seconds;

    return [
      years + ' years',
      days + ' days',
      hours + ' hours',
      minutes + ' min',
      seconds + ' sec'
    ].join(' ');
  }

  closeMyModal() {
    document.querySelector('#add-task').classList.remove('md-show');
  }

  setEditorFocus() {
    this.isEditorFocus = true;
    this.desc = this.project.description;
  }

  saveDesc() {
    this.isEditorFocus = false;
    if (JSON.stringify(this.desc) != JSON.stringify(this.project.description)) {
      this.projectService.Update(this.project)
        .subscribe(
          (res: any) => {},
          (err: any) => console.log(err)
        );
    }
  }

  cancelDesc() {
    this.isEditorFocus = false;
    this.project.description = this.desc;
    this.desc = '';
  }

  addTask() {
    this.newTask = {
      title: '',
      eslimate: '',
      status: 'Open',
      description: ''
    };

    document.querySelector('#add-task').classList.add('md-show');
  }

  createTask() {
    if (this.newTask.taskId) {
      this.projectService.UpdateTask(this.project.projectId, this.newTask)
        .subscribe(
          (res: any) => this.closeMyModal(),
          (err: any) => console.log(err)
        );
    } else {
      this.projectService.AddTask(this.project.projectId, this.newTask)
        .subscribe(
          (res: any) => {
            this.project.tasks = [res, ...this.project.tasks];
            this.closeMyModal();
          },
          (err: any) => console.log(err)
        );
    }
  }

  removeTask(event, task, index) {
    event.stopPropagation();
    event.preventDefault();

    Swal.fire({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(res => {
        if (res.value) {
          if (task.taskId) {
            this.projectService.DeleteTask(this.project.projectId, task.taskId)
              .subscribe(
                (res: any) => {
                  this.project.tasks.splice(index, 1);
                  
                  Swal.fire(
                    'Deleted!',
                    'Your transaction has been deleted.',
                    'success'
                  );
                },
                (err: any) => console.log(err)
              )
          }
        }
      });
  }

  editTask(event, task) {
    event.stopPropagation();
    event.preventDefault();

    this.newTask = task;

    document.querySelector('#add-task').classList.add('md-show');
  }

  doneTask(event, task) {
    event.stopPropagation();
    event.preventDefault();

    if (task.status != 'Completed') {
      task.status = 'Completed';

      this.projectService.UpdateTask(this.project.projectId, task)
        .subscribe(
          (res: any) => {},
          (err: any) => console.log(err)
        );
    }
  }

  checkIn() {
    if (this.project && this.project.projectId && !this.project.active) {
      this.project.active = true;
      this.project.checkIn = new Date().toISOString();
      this.project.status = 'Open';

      this.projectService.Update(this.project)
        .subscribe(
          (res: any) => {
            this.startCounter();
          },
          (err: any) => console.log(err)
        );
    }
  }

  deleteProject(event) {
    event.stopPropagation();
    event.preventDefault();

    if (this.project.projectId) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You wont be able to revert',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      })
        .then(res => {
          if (res.value) {
            this.projectService.Delete(this.project.projectId)
              .subscribe(
                (res: any) => {
                  Swal.fire(
                    'Deleted!',
                    'Your transaction has been deleted.',
                    'success'
                  )
                    .then(res => {
                      if (res.value) {
                        this.router.navigate(['/project']);
                      }
                    });
                },
                (err: any) => console.log(err)
              );
          }
        });
    }
  }

  updateProject() {
    if (this.project.projectId) {
      this.projectService.Update(this.project)
        .subscribe(
          (res: any) => this.editMode = false,
          (err: any) => console.log(err)
        );
    }
  }

  doneProject() {
    if (this.project && this.project.projectId) {
      this.project.status = 'Completed';
      this.project.active = false;
      this.project.completedAt = new Date().toISOString();

      this.projectService.Update(this.project)
        .subscribe(
          (res: any) => this.subscription.unsubscribe(),
          (err: any) => console.log(err)
        );
    }
  }

}
