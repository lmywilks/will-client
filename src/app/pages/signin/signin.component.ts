import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  private cookieValue: any = {};
  loginForm: FormGroup;
  errors: any = {};
  loading: boolean = false;
  returnUrl: string = 'dashboard';

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private cookieService: CookieService,
    private route: ActivatedRoute
  ) {
    if (this.cookieService.check('loginForm')) {
      this.cookieValue = JSON.parse(this.cookieService.get('loginForm'));
    }

    this.loginForm = this.fb.group({
      email: [this.cookieValue.email || '', Validators.required],
      password: [this.cookieValue.password || '', Validators.required],
      remember: [this.cookieValue.remember || '']
    });

    this.route.queryParams
      .subscribe(params => { 
        if (params.returnUrl) {
          this.returnUrl = params.returnUrl;
        }
      });
  }

  ngOnInit() {
  }

  login() {
    if (this.loginForm.status === 'VALID') {
      this.errors = {};
      this.loading = true;

      this.userService.Login(this.loginForm.value)
        .pipe(first())
        .subscribe(
          (res: any) => {
            this.loading = false;
            this.router.navigate([this.returnUrl]);
          },
          err => { 
            this.errors = err.error;
            this.loading = false;
          }
        );
    } else {
      this.errors.general = 'Please fill all field.';
    }
  }

}
