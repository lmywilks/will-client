import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  errors: any = {};
  loading: boolean = false;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router
  ) { 
    this.signupForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      username: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  submit() {
    
    if (this.signupForm.status === 'VALID') {
      this.errors = {};
      this.loading = true;

      this.userService.Signup(this.signupForm.value)
        .pipe(first())
        .subscribe(
          (res: any) => {
            this.loading = false;
            this.router.navigate(['dashboard']);
          },
          (err: any) => {
            this.errors = err.error;
            this.loading = false;
          }
        );
    } else {
      this.errors.general = 'Please fill all fields.';
    }
    
  }


}
