import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserService } from '../services/user.service';
import { LoadingService } from './loading.service';
import { finalize } from 'rxjs/operators';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
    activeRequests: number = 0;

    skipUrls = [
        '(.*)\/api\/todo\/(.*)\/list'
    ];

    constructor(
        private loadingService: LoadingService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let displayLoadingScreen = true;

        for (const skipUrl of this.skipUrls) {
            if (new RegExp(skipUrl).test(request.url)) {
                displayLoadingScreen = false;
                break;
            }
        }

        if (displayLoadingScreen) {
            if (this.activeRequests === 0) {
                this.loadingService.startLoading();
            }

            this.activeRequests++;

            return next.handle(request)
                .pipe(finalize(() => {
                    this.activeRequests--;
                    if (this.activeRequests === 0) {
                        this.loadingService.stopLoading();
                    }
                }));
        } else {
            this.loadingService.stopLoading();
            return next.handle(request);
        }
    }
}