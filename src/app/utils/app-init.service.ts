import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppInitService {

  private readonly API = environment.api_root;

  constructor(private http: HttpClient) { }

  Init() {
 
    return new Promise<void>((resolve, reject) => {
        const token = localStorage.getItem('token');

        if (token) {
          this.http.get(`${ this.API }/user`)
            .subscribe(
              (res: any) => {
                localStorage.setItem('user', JSON.stringify(res));
              }
            );
        }

        resolve();
    });
  }
}
